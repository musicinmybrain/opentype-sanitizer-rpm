%global forgeurl https://github.com/khaledhosny/ots/

Name:           opentype-sanitizer
Version:        8.1.0
%forgemeta
Release:        1%{?dist}
Summary:        Parses and serializes OpenType/WOFF/WOFF2 font files

# In https://github.com/khaledhosny/ots/issues/157 “List all the licenses of
# test fonts,” upstream says, “All the fonts should be OFL or MIT I believe,
# but I think I should double check and list them all.” However, none of the
# test fonts are installed, so they do not affect the license field of this
# package.
License:        BSD
URL:            %{forgeurl}
Source0:        %{forgesource}

# Patch top-level meson.build to allow building with external system
# dependencies instead of bundled meson subprojects or git submodules. This
# adds the meson options external_woff2, external_lz4, and external_gtest. See
# https://github.com/khaledhosny/ots/issues/221 “OTS should be able to be built
# using system dependencies,” and PR
# https://github.com/khaledhosny/ots/pull/223, which offers this patch
# upstream.
Patch0:         %{name}-unbundle-dependencies.patch

BuildRequires:  meson
BuildRequires:  ninja-build
BuildRequires:  gcc-c++
BuildRequires:  pkgconfig(freetype2)
# Note that libbrotlidec, bundled in upstream as a submodule, is only an
# indirect dependency (via libwoff2).
BuildRequires:  pkgconfig(libwoff2dec)
BuildRequires:  pkgconfig(liblz4)
BuildRequires:  pkgconfig(zlib)
BuildRequires:  pkgconfig(gtest)
# The source package includes several dozen sample “good” fonts to provide a
# minimal set of functional tests. To more thoroughly test “good” fonts,
# HowToTest.md says, “install as many as possible TrueType and OpenType fonts.”
# Rather than adding a huge list of BR’s on arbitrary fonts, and trying to
# ensure that the tests find them, we consider that the bundled “good” fonts
# are adequate for validating the package build (as opposed to developing
# opentype-sanitizer itself).

%global common_description %{expand:
The OpenType Sanitizer (OTS) parses and serializes OpenType files (OTF, TTF)
and WOFF and WOFF2 font files, validating them and sanitizing them as it goes.

The C library is integrated into Chromium and Firefox, and also simple command
line tools to check files offline in a Terminal.

The CSS font-face property is great for web typography. Having to use images in
order to get the correct typeface is a great sadness; one should be able to use
vectors.

However, on many platforms the system-level TrueType font renderers have never
been part of the attack surface before, and putting them on the front line is a
scary proposition... Especially on platforms like Windows, where it’s a
closed-source blob running with high privilege.}

%description %{common_description}


%prep
%forgeautosetup -p1
# Remove bundled dependencies
rm -rf subprojects third_party


%build
%meson -Dexternal_woff2=true -Dexternal_lz4=true -Dexternal_gtest=true
%meson_build


%install
%meson_install


%check
%meson_test


%files
%license LICENSE
%doc README.md
%doc docs/*.md

%{_bindir}/ots-*


%changelog
* Sat Nov 28 2020 Benjamin A. Beasley <code@musicinmybrain.net> - 8.1.0-1
- Initial package
